<?php require_once 'section/header.php' ?>

<div class="container">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <h3>Login</h3>
            <hr>
            <div class="col-lg-8 col-lg-offset-2">
                <form action="/learnPhp/login.php" method="post">
                    <div class="form-group">
                        <label for="username">Username :</label>
                        <input type="text" name="username" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="password">Password :</label>
                        <input type="password" name="password" class="form-control">
                    </div>
                    <dive class="checkbox">
                        <label>
                            <input type="checkbox" name="remember" >
                            Remember Me
                        </label>

                    </dive>
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger">Login</button>
                    </div>
                </form>
              <?php if (!is_null($status)): ?>
                  <div class="alert alert-danger">
                    <?= $status ?>
                  </div>
              <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php require_once 'section/footer.php' ?>
