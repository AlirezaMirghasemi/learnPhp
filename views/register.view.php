<?php require_once 'section/header.php' ?>

<div class="container">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <h3>Register</h3>
            <hr>
            <div class="col-lg-8 col-lg-offset-2">
                <form action="/learnPhp/register.php" method="post">
                    <div class="form-group">
                        <label for="name">First Name :</label>
                        <input type="text" name="name" class="form-control" value='<?php old("name"); ?>'>
                    </div>
                    <div class="form-group">
                        <label for="family">Last Name :</label>
                        <input type="text" name="family" class="form-control" value='<?php old("family"); ?>'>
                    </div>
                    <div class="form-group">
                        <label for="username">Username :</label>
                        <input type="text" name="username" class="form-control" value='<?php old("username"); ?>'>
                    </div>
                    <div class="form-group">
                        <label for="email">Email :</label>
                        <input type="email" name="email" class="form-control" value='<?php old("email"); ?>'>
                    </div>
                    <div class="form-group">
                        <label for="password">Password :</label>
                        <input type="password" name="password" class="form-control">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger">Register</button>
                    </div>
                </form>
              <?php if (!is_null($status)): ?>
                  <div class="alert alert-danger">
                    <?= $status ?>
                  </div>
              <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php require_once 'section/footer.php' ?>
