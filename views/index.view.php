<?php
require_once 'section/header.php';
?>
<!--GET AND POST METHOD-->
<!--Get Method-->
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <form action="/learnPhp/index.php" method="get">
                <div class="form-group">
                    <label for="name">Name :</label>
                    <input type="text" name="name" class="form-control">
                </div>
                <div class="form-group">
                    <label for="family">Family :</label>
                    <input type="text" name="family" class="form-control">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-danger">Send By Get Method</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!--Post Method-->
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <form action="/learnPhp/index.php" method="post">
                <div class="form-group">
                    <label for="name">Name :</label>
                    <input type="text" name="name" class="form-control">
                </div>
                <div class="form-group">
                    <label for="family">Family :</label>
                    <input type="text" name="family" class="form-control">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-danger">Send By Post Method</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require_once 'section/footer.php';
?>





