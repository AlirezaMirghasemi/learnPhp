<?php
/**
 * Created by PhpStorm.
 * User: alire
 * Date: 15/06/2017
 * Time: 11:09 PM
 */
session_start();
require_once "database.php";
function isPost() {
  return $_SERVER['REQUEST_METHOD'] == 'POST';
}

function isGet() {
  return $_SERVER['REQUEST_METHOD'] == 'GET';
}

function redirect($location) {
  header("Location: {$location}");
  die;
}

function validation_require($items) {
  $counter_error = 0;
  foreach ($items as $item) {
    if (empty($item)) {
      $counter_error ++;
    }
  }
  return $counter_error == 0;
}

function auth_login($location = 'login.php') {
  if (!isset($_SESSION['username'])):
    $username = $_COOKIE['username'];
    $password = $_COOKIE['password'];
    if (isset($username) && isset($password)):
      $conn = connectToDB();
      $user = userGet($username, $conn);
      if ($user) {
        if ($password == $user->password):
          $_SESSION['username'] = $username;
          redirect("adminPanel.php");
        endif;
      }
      redirect("{$location}");
    endif;
    redirect("{$location}");
  endif;
}

function auth_logout($location = 'index.php') {
  if (isset($_SESSION['username'])):
    redirect("{$location}");
  endif;
}

function old($key) {
  if (!empty($_REQUEST[$key])) {
    return htmlspecialchars($_REQUEST[$key]);
  }
  return '';
}

function validation_email($email) {
  return filter_var($email, FILTER_VALIDATE_EMAIL);

}