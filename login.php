<?php
require_once "function.php";
/**
 * Created by PhpStorm.
 * User: alire
 * Date: 15/06/2017
 * Time: 10:01 PM
 */

auth_logout();
$status = null;
if (isPost()) {
  $username = htmlspecialchars($_POST['username']);
  $password = htmlspecialchars($_POST['password']);
  if (validation_require([$username, $password])) {
    $conn = connectToDB();
    $user = userGet($username, $conn);
    if ($user) {
      if (hash_hmac('sha256', $password, "secret") == $user->password) {
        $_SESSION['username'] = $username;
        if ($_POST['remember'] == true) {
          setcookie("username", $username, time() + 60 * 60 * 24);
          setcookie("password", hash_hmac('sha256', $password, "secret"), time() + 60 * 60 * 24);
        }
        $_SESSION['username'] = $username;
        redirect("adminPanel.php");
      }
    } else {
      $status = "This User Not Exist!";
    }
  } else {
    $status = "Username or Password is EMPTY!";
  }
}
require "views/login.view.php";