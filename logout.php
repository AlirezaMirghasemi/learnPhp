<?php
/**
 * Created by PhpStorm.
 * User: alire
 * Date: 15/06/2017
 * Time: 10:51 PM
 */
require_once "function.php";
session_destroy();

setcookie("username", '', time() - 60 * 60 * 24);
setcookie("password", '', time() - 60 * 60 * 24);

redirect("index.php");