<?php
/**
 * Created by PhpStorm.
 * User: alire
 * Date: 16/06/2017
 * Time: 10:56 AM
 */
//FILE-----------------------------------------------------------------------------------
//define("LOCATION_DATABASE", "database.txt");
//function getDataOfDatabase() {
//  $handle = file_get_contents(LOCATION_DATABASE);
//  $userse = explode("\n", $handle);
//  $users = [];
//  foreach ($userse as $item) {
//    $u = explode(",", $item);
//    $users[$u[0]] = $u[1];
//  }
//  return $users;
//}
//
//function saveData($data) {
//  file_put_contents(LOCATION_DATABASE, "\n" . $data, FILE_APPEND);
//}
//--------------------------------------------------------------------------------------------
//PDO-----------------------------------------------------------------------------------------
function connectToDB() {
  try {
    return new PDO("mysql:host=localhost;dbname=learnphp", "root", "212963814");
  } catch (PDOException $e) {
    die($e->getMessage());
  }
}

function userGet($username, $conn) {
  $statement = $conn->prepare("SELECT * FROM `users` where `username` = :username");
  $statement->bindparam("username", $username);
  $statement->execute();
  $user = $statement->fetch(PDO::FETCH_OBJ);
  return $user ? $user : false;
}
function emailGet($email,$conn){
  $statement = $conn->prepare("SELECT * FROM `users` where `email` = :email");
  $statement->bindparam("email", $email);
  $statement->execute();
  $emailAddress = $statement->fetch(PDO::FETCH_OBJ);
  return $emailAddress ? $emailAddress : false;
}
function userSave($data, $conn) {
  extract($data);
  $statement = $conn->prepare("INSERT INTO users (fullname,username,email,password) VALUES (:fullname , :username , :email , :password)");
  $statement->bindparam('fullname', $fullName);
  $statement->bindparam('username', $username);
  $statement->bindparam('email', $email);
  $statement->bindparam('password', $password);
  return $statement->execute() ? true : false;
}
//--------------------------------------------------------------------------------------------