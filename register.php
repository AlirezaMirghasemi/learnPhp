<?php
/**
 * Created by PhpStorm.
 * User: alire
 * Date: 16/06/2017
 * Time: 12:15 PM
 */
require_once "function.php";
auth_logout();
$status = null;
if (isPost()) {
  $name = htmlspecialchars($_POST['name']);
  $family = htmlspecialchars($_POST['family']);
  $username = htmlspecialchars($_POST['username']);
  $email = htmlspecialchars($_POST['email']);
  $password = htmlspecialchars($_POST['password']);
  if (validation_require([$name, $family, $username, $email, $password])) {
    if (validation_email($email)) {
      $data = [
        "fullName" => $name . ' ' . $family,
        "username" => $username,
        "email" => $email,
        "password" => hash_hmac('sha256', $password, "secret")
      ];
      $conn = connectToDB();
      if (!userGet($username, $conn)) {
        if (!emailGet($email, $conn)) {
          userSave($data, $conn) ? redirect('login.php') : $status = "User Not Save Please Try Again!";
        } else {
          $status = "Email is already exists!";
        }
      } else {
        $status = "Username is already exists!";
      }
    } else {
      $status = "Insert Valid Email!";
    }
  } else {
    $status = "Fill All Required Data!";
  }
}
require "views/register.view.php";