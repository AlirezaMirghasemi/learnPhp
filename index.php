<?php
require_once "function.php";
//GET AND POST METHOD------------------------------
if (isGet() && !empty($_GET)):
  $name=htmlspecialchars($_GET['name']);
  $family=htmlspecialchars($_GET['family']);
  if (validation_require([$name, $family])):
    echo "{$name} {$family} <p>GET Method!</p>";
  endif;
elseif (isPost() && !empty($_POST)):
  $name=htmlspecialchars($_POST['name']);
  $family=htmlspecialchars($_POST['family']);
  if (validation_require([$name, $family])):
    echo "{$name} {$family} <p>POST Method!</p>";
  endif;
endif;
//------------------------------------------------

require "views/index.view.php";
?>